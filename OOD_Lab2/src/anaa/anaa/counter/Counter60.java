//Git and Eclipse are driving me up a wall. 

package anaa.counter;

import anaa.counter.AbstractCounter.Direction;

public class Counter60 extends SettableCounter {

		public Counter60() 
		{
			this(null, null);
			//What's being called here?
			// > The constructor of AbstractCounter
			// But why null?
			// In case we don't have a referred name or connected counter.
			// In that case, we don't have to call another object
		}

		public Counter60(String counterName) 
		{
			super(counterName, 60, Direction.INCREASING);
		} 

		public Counter60(String counterName, CounterType next) {
			super(counterName, 60, next, Direction.INCREASING);
			//What are we doing here, really?
			//Calling the constructor of AbstractCounter, sending in:
			//countSpace, an int
			//an Counter object, called nextCounter
			//A direction enumeral, which says it's going to be an increasing counter
		}
		
		@Override
		public String toString()
		{
			return String.format("%2d", countedValue);
		}
}
