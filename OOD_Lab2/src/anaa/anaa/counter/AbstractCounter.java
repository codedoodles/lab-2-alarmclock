package anaa.counter;

public abstract class AbstractCounter implements CounterType
{
	public enum Direction {INCREASING, DECREASING};
	protected int countedValue;		//If we're going to access it in specific counter classes, we can't have it private.
	private boolean isPaused;
	private final boolean IS_CIRCULAR;
	private final int COUNT_SPACE;
	private Direction direction;
	private CounterType nextCounter; //Why are you complaining? You're being used down there!
	private String counterName; //I want to see the name of the counter in printout. 
	public AbstractCounter (String counterName, int countSpace, CounterType nextCounter, Direction direction) 
	{
		if(countSpace >= 2) // Why 2?!
		{
			this.COUNT_SPACE = countSpace;
			this.IS_CIRCULAR = true;
		}
		else 
		{
			this.COUNT_SPACE = 0;
			this.IS_CIRCULAR = false; //Why!? 
		}
		this.counterName = counterName; 
		this.direction = direction;
		this.nextCounter = nextCounter;
	}
	
	public AbstractCounter (String counterName, int countSpace, Direction direction)
	{
		this(counterName, countSpace, null, direction);
	}
	
	public void setDirection(Direction direction) 
	{
		this.direction = direction;
	}
	
	@Override
	public int getCount() {
		return countedValue; }
	
	@Override
	public void reset() 
	{ 
		countedValue = 0;
	}
	
	@Override
	public void pause() 
	{ 
		isPaused = true;
	}
	
	@Override
	public void resume() 
	{
		isPaused = false;
	}
	
	@Override
	public void count() 
		{ 
		if (!isPaused) 
			{
				if(direction == Direction.INCREASING)
				{
					countedValue++;
					System.out.println(this.counterName + ":" + this.getCount());
					
					/* Skicka till nästa counter >  Resetta till 0 */
					if(IS_CIRCULAR && countedValue >= COUNT_SPACE)
					{
						
						if(nextCounter != null) {
							nextCounter.count();
							//System.out.println(nextCounter);
						}
						
						else {
							System.out.println("No need to forward "+ this.counterName + " counter!");
						}
						
						System.out.println("Let's reset the " + this.counterName + " counter now! ");
						this.reset();
					}
				}
				else if (direction == Direction.DECREASING)
				{
					countedValue--;
				}
			}
		}
	@Override
	public String toString()
	{
		return "Um, something should come out here." + getCount();
		/*???*/
	}
}
