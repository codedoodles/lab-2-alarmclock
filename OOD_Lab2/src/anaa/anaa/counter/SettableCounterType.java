/**
 * 
 */
package anaa.counter;

/**
 * @author 18anaa01
 *
 */
public interface SettableCounterType extends CounterType{

	public void setCount(int value);
}
