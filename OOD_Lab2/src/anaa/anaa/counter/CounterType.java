package anaa.counter;
//Interface is a pre-defined abstract class. 

public interface CounterType {

	int getCount();

	void reset();

	void pause();

	void resume();

	void count();

}
