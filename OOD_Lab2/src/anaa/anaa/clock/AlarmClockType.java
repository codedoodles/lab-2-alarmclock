/**
 * 
 */
package anaa.clock;
import java.util.Collection;
import anaa.alarm.AlarmType;
import anaa.time.TimeType;
/**
 * @author anous
 *
 */
public interface AlarmClockType {
	  
		  public void tickTack();
		  public void setTime(TimeType time);
		  public void addAlarm(AlarmType larm);
		  public void removeAlarm(AlarmType alarm);
		  public Collection<AlarmType> getAlarms();
		  public TimeType getTime();
		  public String toString();
		  
}
