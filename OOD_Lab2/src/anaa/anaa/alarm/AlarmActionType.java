/**
 * 
 */
package anaa.alarm;
import anaa.time.TimeType;

/**
 */
public interface AlarmActionType {
	public void setActive(boolean active);
	public boolean isActive();
	public void setTime(TimeType time);
	public TimeType getTime();
	public void addAlarmActionHandler(AlarmActionType handler);
	public void doAlarm();
	
	public void alarmActivated();
	public void alarmDeactivated();
}
