/**
 * 
 */
package anaa.time;

/**
 * @author anous
 * Ni får gärna använda en färdigimplementation av detta interface 
 * eftersom det är ganska krångligt att effektivt konvertera mellan representationer 
 * som textsträngar och heltal. 
 * Se given kod Time.java bilaga 1
 */
public interface TimeType {
	public int getSecond();
	public void setSecond(int second);
	public int getMinute();
	public void setMinute(int minute);
	public int getHour();
	public void setHour(int hour);
	public int getDay();
	public void setDay(int day);
	public boolean hasDay();
	public String toString();
	}

